import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  nuevasPeliculas: any[] = [];
  loading: boolean;

  constructor(private encontrarPeli: Router) { }

  ngOnInit() {
  }

  buscarPeli(form: any) {
    console.log(form);
    return this.encontrarPeli.navigate(['/results', form.value.buscar]);
  }

}
