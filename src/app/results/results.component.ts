import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculasServicioService } from '../servicios/peliculas-servicio.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  title: string;
  movies: any = {};

  constructor(private activatedroute: ActivatedRoute, private _pelisService: PeliculasServicioService, private detallePeli: Router) {
    this.activatedroute.params.subscribe(data => {
      this.title = data['title'];
      this._pelisService.getBuscar(this.title).subscribe(datos => {
        console.log(datos);
        this.movies = datos;
      });
    });
  }

  ngOnInit() {
  }
  iraPeli(slug: string) {
    this.detallePeli.navigate(['/details', slug]);
  }
}

