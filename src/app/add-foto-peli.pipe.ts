import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addFotoPeli'
})
export class AddFotoPeliPipe implements PipeTransform {
  imagePathUrl = 'https://image.tmdb.org/t/p/w300/';

  transform(value: string): any {
    return this.imagePathUrl + value;
  }

}
