import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { PeliculasServicioService } from '../servicios/peliculas-servicio.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {

  nuevasPeliculas: any[] = [];
  nuevasPeliculasAccion: any [] = [];
  loading: boolean;

  constructor(private movieDB: PeliculasServicioService, private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.movieDB.getPopulares()
      .subscribe((data: any) => {

        // console.log(data);
        this.nuevasPeliculas = data['results']; //results hace referencia a los datos del array, llamados así
        this.loading = false;
      });

    this.movieDB.getPopularesAccion()
      .subscribe((data: any) => {

        // console.log(data);
        this.nuevasPeliculasAccion = data['results']; //results hace referencia a los datos del array, llamados así
        this.loading = false;
      });
  }

  iraPeli(slug: string) {
    this.router.navigate(['/details', slug]);
  }
}
