import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { SlickModule} from 'ngx-slick';


@Component({
  selector: 'app-carrouselpeli',
  templateUrl: './carrouselpeli.component.html',
  styleUrls: ['./carrouselpeli.component.css']
})
export class CarrouselpeliComponent implements OnInit {

  @Input() arrayNuevasPelis: any;
  @Output() pelisId: EventEmitter<any> = new EventEmitter<any>();

  slideconfig = {
    slideToShow:4,
    slideToScroll:4,
    dots: true,
    infinite: false,
    // nextArrow: '<div class=\'nav-btn next-slide\'></div>',
    // prevArrow: '<div class=\'nav-btn prev-slide\'></div>',
  };
  constructor() { }

  ngOnInit() {

  }

  detallesId(id: any) {
    this.pelisId.emit(id);

  }
}
