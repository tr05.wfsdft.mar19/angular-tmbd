import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrouselpeliComponent } from './carrouselpeli.component';

describe('CarrouselpeliComponent', () => {
  let component: CarrouselpeliComponent;
  let fixture: ComponentFixture<CarrouselpeliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrouselpeliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrouselpeliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
