import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculasServicioService } from '../../servicios/peliculas-servicio.service';

@Component({
  selector: 'app-pelicula-detail',
  templateUrl: './pelicula-detail.component.html',
  styleUrls: ['./pelicula-detail.component.css']
})
export class PeliculaDetailComponent implements OnInit {

  slug: string;
  movie: any = {};

  constructor(private activatedRoute: ActivatedRoute, private pelisService: PeliculasServicioService) {
    this.activatedRoute.params.subscribe(data => {
      this.slug = data['slug'];

      this.pelisService.getMovieById(this.slug).subscribe(datos => {
        this.movie = datos;
      });

  });
  }

  ngOnInit() {
  }
  goBack() {
    window.history.back();
  }
}
