import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PeliculasServicioService {

  private urlMovieDB = 'https://api.themoviedb.org/3/';
  private apikey = '8ba7db603b587bb260006a85bb3d6215';
  slug = 'poster_path';
  constructor(private http: HttpClient) { }
  // Método para pasar la petición por la URL
  getPopulares() {
    // any [] devuelve un array de cualquier tipo
    const url =  `${this.urlMovieDB}discover/movie?sort_by=popularity.desc&api_key=${this.apikey}`;
    return this.http.get<any[]>(url);
  }
  // Método para pasar la peticion de la imagen por la URL
  getMovieById(movie_ID) {
    const url = `${this.urlMovieDB}movie/${movie_ID}?api_key=${this.apikey}`;
    return this.http.get<any[]>(url);
  }
  getBuscar(peliBuscada) {
    const url = `${this.urlMovieDB}search/multi?query=${peliBuscada}&api_key=${this.apikey}`;
    return this.http.get<any[]>(url);
  }
  getPopularesAccion() {
    const url =  `${this.urlMovieDB}discover/movie?with_genres=28&sort_by=vote_average.desc&api_key=${this.apikey}`;
    return this.http.get<any[]>(url);
  }
}



