import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SlickModule } from 'ngx-slick';


// Más modulos
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

// Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { Error404Component } from './error404/error404.component';
import { PeliculaDetailComponent } from './peliculas/pelicula-detail/pelicula-detail.component';
import { SearchComponent } from './search/search.component';


// Servicios
import { PeliculasServicioService } from './servicios/peliculas-servicio.service';

// Pipes
import { AddFotoPeliPipe } from './add-foto-peli.pipe';
import { ResultsComponent } from './results/results.component';
import { CarrouselpeliComponent } from './peliculas/carrouselpeli/carrouselpeli.component';





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    PeliculasComponent,
    Error404Component,
    AddFotoPeliPipe,
    PeliculaDetailComponent,
    SearchComponent,
    ResultsComponent,
    CarrouselpeliComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    SlickModule.forRoot()
  ],
  providers: [
    PeliculasServicioService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
