import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { Error404Component } from './error404/error404.component';
import { PeliculaDetailComponent } from './peliculas/pelicula-detail/pelicula-detail.component';
import { ResultsComponent } from './results/results.component';

import {CommonModule} from '@angular/common';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'peliculas', component: PeliculasComponent},
  {path: '404', component: Error404Component},
  {path: 'details/:slug', component: PeliculaDetailComponent},
  {path: 'results/:title', component: ResultsComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [
    [RouterModule.forRoot(routes)],
    CommonModule
  ],
  exports: [
    RouterModule],
  declarations: []
})
export class AppRoutingModule { }
